<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2012                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_signature_charger_dist($id_article) {
	$vars = array('session_nom','session_email','firstname','lastname','adresse1','adresse2','codepostal','ville','organisation','fonction','entreprise','profession');
	foreach ($vars as $var) {
		if (!isset($GLOBALS['visiteur_session'][$var])) $GLOBALS['visiteur_session'][$var]=null;
	}

	include_spip('base/abstract_sql');
	// pas de petition, pas de signature
	if (!$r = sql_fetsel('*','spip_petitions','id_article='.intval($id_article)))
		return false;

	// pas de signature sur une petition fermee (TODO) ou poubelle
	if (isset($r['statut']) AND in_array($r['statut'],array('off','poubelle')))
		return false;
	$id_petition = $r['id_petition'];
	
	if (!$login) {
		$login = strval(_request('var_login'));
	}
	// si on est deja identifie
	if (!$login and isset($GLOBALS['visiteur_session']['email'])) {
		$login = $GLOBALS['visiteur_session']['email'];
	}
	if (!$login and isset($GLOBALS['visiteur_session']['login'])) {
		$login = $GLOBALS['visiteur_session']['login'];
	}
	// ou si on a un cookie admin
	if (!$login) {
		if (isset($_COOKIE['spip_admin'])
			and preg_match(',^@(.*)$,', $_COOKIE['spip_admin'], $regs)
		) {
			$login = $regs[1];
		}
	}
	
	if ($login) {
		$login_nom = session_get('nom');
		$login_email = session_get('email');		
	}
	

	$valeurs = array(
		'id_petition' => $id_petition,
		'session_nom' => sinon($GLOBALS['visiteur_session']['session_nom'],
			$GLOBALS['visiteur_session']['nom']),
		'session_email'=> sinon($GLOBALS['visiteur_session']['session_email'],
			$GLOBALS['visiteur_session']['email']),
		'signature_nom_site'=>'',
		'signature_url_site'=>'http://',
		'_texte'=>$r['texte'],
		'_message'=>($r['message'] == 'oui') ? ' ':'',
		'message'=>'',
		'site_obli' => '', // Plus de site dans le formulaire --- ($r['site_obli'] == 'oui'?' ':''),
		'firstname' => sinon(_request('firstname'),$GLOBALS['visiteur_session']['firstname']),
		'lastname' => sinon(_request('lastname'),$GLOBALS['visiteur_session']['lastname']),
		'adresse1' => sinon(_request('adresse1'),$GLOBALS['visiteur_session']['adresse1']),
		'adresse2' => sinon(_request('adresse2'),$GLOBALS['visiteur_session']['adresse2']),
		'codepostal' => sinon(_request('codepostal'),$GLOBALS['visiteur_session']['codepostal']),
		'ville' => sinon(_request('ville'),$GLOBALS['visiteur_session']['ville']),
		'organisation' => sinon(_request('organisation'),$GLOBALS['visiteur_session']['organisation']),
		'fonction' => sinon(_request('fonction'),$GLOBALS['visiteur_session']['fonction']),
		'entreprise' => sinon(_request('entreprise'),$GLOBALS['visiteur_session']['entreprise']),
		'profession' => sinon(_request('profession'),$GLOBALS['visiteur_session']['profession']),
		'message' => _request('message'),
		'login' => $login,
		'login_nom' => $login_nom,
		'login_email' => $login_email,
		'debut_signatures'=>'' // pour le nettoyer de l'url d'action !
	);
	if (isset($GLOBALS['visiteur_session']['statut']) AND in_array($GLOBALS['visiteur_session']['statut'],array('0minirezo','1auteur'))) {
		$valeurs['nom_porteur'] = $GLOBALS['visiteur_session']['session_nom'];
		$valeurs['email_porteur'] = $GLOBALS['visiteur_session']['session_email'];
	}

	if ($c = _request('var_confirm')) {
		$valeurs['_confirm'] = $c;
		$valeurs['editable'] = false;
	}

	return $valeurs;
}

function affiche_reponse_confirmation($confirm) {
	if (!$confirm) return '';
	$confirmer_signature = charger_fonction('confirmer_signature','action');
	return $confirmer_signature($confirm);  # calculee plus tot: cf petitions_options
}

function formulaires_signature_verifier_dist($id_article) {
	$erreurs = array();
	$oblis = array('firstname','lastname','codepostal','ville');
	
	include_spip('base/abstract_sql');
	$row = sql_fetsel('*','spip_petitions','id_article='.intval($id_article));
	if (!$row)
		$erreurs['message_erreur'] = _T('form_pet_probleme_technique');
	$id_petition = $row['id_petition'];

	foreach($oblis as $champ) {
		if (!_request($champ)) {
			$erreurs[$champ] = _T('mespetits:champ_obligatoire');
			$erreurs['message_erreur'] = _T('mespetits:certains_champs_manquants').$champ;
		}
	}
	
	if ($nom = _request('session_nom') AND strlen($nom) < 2)
		$erreurs['session_nom'] =  _T('form_indiquer_nom');
	else 
		$nom = _request('firstname') . " " . _request('lastname');

	include_spip('inc/filtres');
	
	$mail=_request('session_email');
	if (!isset($GLOBALS['visiteur_session']['id_auteur'])) {$GLOBALS['visiteur_session']['id_auteur']=null;}
	// Ne vérifier le mail que dans UN cas : utilisateur non connecté ou visiteur
	if ((!$GLOBALS['visiteur_session']['id_auteur'] OR $GLOBALS['visiteur_session']['statut'] == '6forum') 
		AND (!$mail OR ($mail == _T('info_mail_fournisseur'))))
		$erreurs['adresse_email'] = "Vous devez indiquer une adresse email";
	elseif ($mail AND !email_valide($mail)) 
		$erreurs['adresse_email'] = "Vous devez indiquer une adresse email valide";
	elseif (strlen(_request('nobot'))
		OR (@preg_match_all(',\bhref=[\'"]?http,i', // bug PHP
				    _request('message') 
				    # ,  PREG_PATTERN_ORDER
				   )
		    >2)) {
		#$envoyer_mail = charger_fonction('envoyer_mail','inc');
		#envoyer_mail('email_moderateur@example.tld', 'spam intercepte', var_export($_POST,1));
		$erreurs['message_erreur'] = _T('form_pet_probleme_liens'.' BOT : '.print_r(_request('nobot')).' / '.$message);
	}
	
	$doublon = 'F:'._request('firstname').'- L:'._request('lastname').'- CP:'._request('codepostal');
	$doublon = 'F:'._request('firstname').'- L:'._request('lastname').'- CP:'._request('codepostal');
// ajout d'un test sur id petition,sinon, on ne peut en signer qu'une seule...	?
//	$n = sql_fetsel('*', 'spip_signatures', "statut!='poubelle' AND CONCAT('F:',firstname,'- L:',lastname,'- CP:',codepostal) = ".sql_quote($doublon));
	$n = sql_fetsel('*', 'spip_signatures', "statut!='poubelle' AND id_petition = $id_petition AND CONCAT('F:',firstname,'- L:',lastname,'- CP:',codepostal) = ".sql_quote($doublon));

	if ($n) {
		spip_log("doublons $n  : $doublon");
		$erreurs['message_erreur'] = "Erreur : Doublon détecté ! Avez-vous déja signé cette pétition ? sinon, ajouter un numéro au prénom...";}
// fin modif pam

	if (!count($erreurs)){
		// tout le monde est la.
		$email_unique = $row['email_unique']  == "oui";
		$site_unique = $row['site_unique']  == "oui";
	
		// Refuser si deja signe par le mail ou le site quand demande
		// Il y a un acces concurrent potentiel,
		// mais ca n'est qu'un cas particulier de qq n'ayant jamais confirme'.
		// On traite donc le probleme a la confirmation.
	
		if ($email_unique) {
			$r = sql_countsel('spip_signatures', "id_petition=".intval($id_petition)." AND ad_email=" . sql_quote($mail) . " AND statut='publie'");
			if ($r)	$erreurs['message_erreur'] =  _T('form_pet_deja_signe');
		}

	}

	return $erreurs;
}

function formulaires_signature_traiter_dist($id_article) {

	$reponse = '<hr>Erreur signature_traiter_dist:144 : '._T('form_pet_probleme_technique');
	include_spip('base/abstract_sql');
	if (spip_connect()) {
		$controler_signature = charger_fonction('controler_signature', 'inc');
		$reponse = $controler_signature($id_article,
			_request('firstname'),_request('lastname'),
			_request('session_nom'), _request('session_email'),
			_request('message'), _request('adresse1'),
			_request('adresse2'), _request('codepostal'),
			_request('ville'), _request('organisation'),
			_request('fonction'), _request('entreprise'),
			_request('profession'), _request('url_page'));

	}

	return array('message_ok'=>$reponse);
}

// Retour a l'ecran du lien de confirmation d'une signature de petition.
// Si var_confirm est non vide, c'est l'appel dans public/assembler.php
// pour vider le cache au demarrage afin que la nouvelle signature apparaisse.
// Sinon, c'est l'execution du formulaire et on retourne le message 
// de confirmation ou d'erreur construit lors de l'appel par assembler.php
// Le controle d'unicite du mail ou du site (si requis) refait ici correspond
// au cas de mails de demande de confirmation laisses sans reponse

// http://doc.spip.org/@reponse_confirmation_dist
/*function formulaires_signature_reponse_confirmation_dist($var_confirm = '') {
	static $confirm = null;

	// reponse mise en cache dans la session ?
	$code_message = 'signature_message_'.strval($var_confirm);
	if (isset($GLOBALS['visiteur_session'][$code_message]))
		return $GLOBALS['visiteur_session'][$code_message];

	// reponse deja calculee depuis public/assembler.php
	if (isset($confirm))
		return $confirm;

	if ($var_confirm == 'publie' OR $var_confirm == 'poubelle')
		return '';

	if (!spip_connect()) {
		$confirm = _T('form_pet_probleme_technique').print_r(debug_backtrace(),1);
		return '';
	}
	include_spip('inc/texte');
	include_spip('inc/filtres');

	// Suppression d'une signature par un moderateur ?
	// Cf. plugin notifications
	if (isset($_GET['refus'])) {
		// verifier validite de la cle de suppression
		// l'id_signature est dans var_confirm
		include_spip('inc/securiser_action');
		if ($id_signature = intval($var_confirm)
		    AND (
			$_GET['refus'] == _action_auteur("supprimer signature $id_signature", '', '', 'alea_ephemere')
				OR
			$_GET['refus'] == _action_auteur("supprimer signature $id_signature", '', '', 'alea_ephemere_ancien')
			)) {
			sql_updateq("spip_signatures", array("statut" => 'poubelle'), "id_signature=$id_signature");
			$confirm = _T('info_signature_supprimee');
		} else $confirm = _T('info_signature_supprimee_erreur');
		return '';
	}

	$row = sql_fetsel('*', 'spip_signatures', "statut=" . sql_quote($var_confirm), '', "1");

	if (!$row) {
		$confirm = _T('form_pet_aucune_signature');
		return '';
	}

	$id_signature = $row['id_signature'];
	$id_signature = $row['id_signature'];
	$adresse_email = $row['ad_email'];
	$url_site = $row['url_site'];

	$row = sql_fetsel('email_unique, site_unique, id_liste', 'spip_petitions', "id_signature=$id_signature");

	$email_unique = $row['email_unique']  == "oui";
	$site_unique = $row['site_unique']  == "oui";
	$id_liste = $row['id_liste'];

	sql_updateq('spip_signatures',
		    array('statut' => 'publie', 'date_time' => date('Y-m-d H:i:s')),
		    "id_signature=$id_signature");

	if ($email_unique) {

		$r = "id_signature=$id_signature AND ad_email=" . sql_quote($adresse_email);
		if (signature_entrop($r))
			  $confirm =  _T('form_pet_deja_signe');
	} 

	if ($site_unique) {
		$r = "id_signature=$id_signature AND url_site=" . sql_quote($url_site);
		if (signature_entrop($r))
			$confirm = _T('form_pet_site_deja_enregistre');
	}

	include_spip('inc/session');

	if (!$confirm) {
		$confirm = _T('form_pet_signature_validee');

		// noter dans la session que l'email est valide
		// de facon a permettre de signer les prochaines
		// petitions sans refaire un tour d'email
		session_set('email_confirme', $adresse_email);


		// invalider les pages ayant des boucles signatures
		include_spip('inc/invalideur');
		suivre_invalideur("id='varia/pet$id_signature'");
	}

	// Conserver la reponse dans la session du visiteur
	if ($confirm)
		session_set($code_message, $confirm);
}
*/

//
// Recevabilite de la signature d'une petition
// les controles devraient mantenant etre faits dans formulaires_signature_verifier()
// 

// http://doc.spip.org/@inc_controler_signature_dist
function inc_controler_signature_dist($id_article, $firstname, $lastname, $nom, $mail, $message, 
			$adresse1, $adresse2, $codepostal, $ville, $organisation, $fonction, $entreprise, $profession, $url_page) {

	include_spip('inc/texte');
	include_spip('inc/filtres');

	// tout le monde est la.
	// cela a ete verifie en amont, dans formulaires_signature_verifier()
	if (!$row = sql_fetsel('*', 'spip_petitions', "id_article=$id_article"))
		return _T('form_pet_probleme_technique').'<pre>1 >>'.print_r(debug_backtrace(),1);

	$statut = "";
	$champs = array(
		'firstname' => $firstname,
		'lastname' => $lastname,
		'adresse1' => $adresse1,
		'adresse2' => $adresse2,
		'codepostal' => $codepostal,
		'organisation' => $organisation,
		'fonction' => $fonction,
		'entreprise' => $entreprise,
		'profession' => $profession,
		'ville' => $ville,
		'message' => $message,
		'ip' => $_SERVER["REMOTE_ADDR"]);
		

	if (!$ret = signature_a_confirmer($id_article, $url_page, $nom, $mail, $champs, $lang, $statut))
		return _T('form_pet_probleme_technique').'<pre>2 ====>> '.print_r(debug_backtrace(),1);

	include_spip('action/editer_signature');

	$id_signature = signature_inserer($row['id_petition']);
	if (!$id_signature) return _T('form_pet_probleme_technique');

	signature_modifier($id_signature,
		array(
		'statut' => $statut,
		'nom_email' => $nom,
		'ad_email' => $mail,
		'message' => $message,
		'nom_site' => $site,
		'url_site' => $url_site
		)
	);

	if (isset($GLOBALS['visiteur_session']['id_auteur'])) {
		$champs['id_porteur'] = $GLOBALS['visiteur_session']['id_auteur'];
	}
	
	sql_updateq('spip_signatures', $champs,
	'id_signature='.$id_signature
	);

	if ($ret == _T('form_pet_signature_validee')) {
		$abonner_auteur_apres_signature = charger_fonction('abonner_auteur_apres_signature','inc');
		$abonner_auteur_apres_signature($mail,$nom,$row['id_liste']);		
	}
// pam 20150818..ajout $nom dans appel $abonner_auteur_apres_signature
	return $ret;
}

// http://doc.spip.org/@signature_a_confirmer
function signature_a_confirmer($id_article, $url_page, $nom, $mail, $champs, $lang, &$statut)
{

	// Si on est deja connecte et que notre mail a ete valide d'une maniere
	// ou d'une autre, on entre directement la signature dans la base, sans
	// envoyer d'email. Sinon email de verification
	if (
		// Cas 1: on est loge (et on signe avec N'IMPORTE QUEL MAIL)
		(
		isset($GLOBALS['visiteur_session']['statut'])
		//AND $GLOBALS['visiteur_session']['session_email'] == $GLOBALS['visiteur_session']['email']
		//AND strlen($GLOBALS['visiteur_session']['email'])
		)

		// Cas 2: on a deja signe une petition, et on conserve le meme email
		OR (
		isset($GLOBALS['visiteur_session']['email_confirme'])
		AND $GLOBALS['visiteur_session']['session_email'] == $GLOBALS['visiteur_session']['email_confirme']
		AND strlen($GLOBALS['visiteur_session']['session_email'])
		)
	) {
		// Si on est en ajax on demande a reposter sans ajax, car il faut
		// recharger toute la page pour afficher la signature
		refuser_traiter_formulaire_ajax();

		$statut = 'publie';
		// invalider le cache !
		include_spip('inc/invalideur');
		suivre_invalideur("id='article/$id_article'");

		// message de reussite : en ajax, preciser qu'il faut recharger la page
		// pour voir le resultat
		return
			_T('mespetits:form_pet_signature_validee');
	}

	//
	// Cas normal : envoi d'une demande de confirmation
	//
	$row = sql_fetsel('titre,lang', 'spip_articles', "id_article=".intval($id_article));
	$lang = lang_select($row['lang']);
	$titre = textebrut(typo($row['titre']));
	if ($lang) lang_select();

	if (!strlen($statut))
		$statut = mespetits_signature_test_pass();

	if ($lang != $GLOBALS['meta']['langue_site'])
		  $url_page = parametre_url($url_page, "lang", $lang,'&');

	$url_page = parametre_url($url_page, 'var_confirm', $statut, '&')
	. "#sp$id_article";

	list($sujet, $corps) =  signature_demande_confirmation($id_article, $url_page, $nom, $champs, $titre, $statut);

	$envoyer_mail = charger_fonction('envoyer_mail','inc');
	if ($envoyer_mail($mail, $sujet, $corps))
		return _T('mespetits:form_pet_envoi_mail_confirmation',array('email'=>$mail));
	else
		die('Envoyer_mail pb : '.$envoyer_mail);

	return false; # erreur d'envoi de l'email
}

function signature_langue($id_article, $url_page)
{
	$row = sql_fetsel('titre,lang', 'spip_articles', "id_article=$id_article");
	$lang = lang_select($row['lang']);
	$titre = textebrut(typo($row['titre']));

	if ($lang) lang_select();
	if ($lang != $GLOBALS['meta']['langue_site'])
		  $url_page = parametre_url($url_page, "lang", $lang,'&');

	return array($titre, $url_page);
}

function signature_demande_confirmation($id_article, $url_page, $nom, $champs, $titre, $statut)
{

$detail = "Pr&eacute;nom : {$champs['firstname']}
Nom : {$champs['lastname']}
Adresse : {$champs['adresse1']}
{$champs['adresse2']}
Code Postal : {$champs['codepostal']}
Organisation : {$champs['organisation']}
Fonction : {$champs['fonction']}
Entreprise : {$champs['entreprise']}
Profession : {$champs['profession']}
Ville : {$champs['ville']}
Adresse IP : {$champs['ip']}";

	return array(_T('mespetits:form_pet_confirmation')." ". $titre, 
		     _T('mespetits:form_pet_mail_confirmation',
			 array('titre' => $titre,
			       'nom_email' => $nom,
			       'detail' => $detail, 
			       'url' => $url_page,
			       'message' => $msg)));
}

// Creer un mot de passe aleatoire et verifier qu'il est unique
// dans la table des signatures
// http://doc.spip.org/@signature_test_pass
function mespetits_signature_test_pass() {
	include_spip('inc/acces');
	do {
		$passw = creer_pass_aleatoire();
	} while (sql_countsel('spip_signatures', "statut='$passw'") > 0);

	return $passw;
}
