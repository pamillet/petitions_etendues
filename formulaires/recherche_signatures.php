<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_recherche_signatures_charger_dist() {
	

	#Recherche dans les supprimées etc..
	$statut = sinon(_request('statut'),"publie");
	if ($statut != "tous") $where = "{$GLOBALS['connexions'][0]['prefixe']}_signatures.statut='".$statut."' ";
	else $where = " '1'='1' ";


	if (_request("formulaire_action") == "recherche_signatures") {

		# Valeurs par défaut
		foreach(array('ville','codepostal','departement','id_porteur','date_min_signature','date_max_signature') as $env)
			$valeurs[$env] = _request($env);
			
		#Traiter les requetes et affiner les autres criteres
		$awhere = array();
		if ($valeurs['id_porteur']) $awhere[] = 'id_porteur='.sql_quote($valeurs['id_porteur']);
		if ($valeurs['ville']) $awhere[] = "{$GLOBALS['connexions'][0]['prefixe']}_signatures.ville = ".sql_quote($valeurs['ville']);
		if ($valeurs['codepostal']) {
			foreach($valeurs['codepostal'] as $code) {$liste_cp[] = sql_quote($code);}
			$awhere[] = "{$GLOBALS['connexions'][0]['prefixe']}_signatures.codepostal IN (" . implode(', ', $liste_cp) . ")";
		}
		if ($valeurs['departement']) $awhere[] = "{$GLOBALS['connexions'][0]['prefixe']}_signatures.codepostal = ".sql_quote($valeurs['departement'] );
		if ($valeurs['date_min_signature']) $awhere[] = 'date_time >= STR_TO_DATE('.sql_quote(str_replace('/','.',$valeurs['date_min_signature'])).', GET_FORMAT(DATE, "EUR"))';
		if ($valeurs['date_max_signature']) $awhere[] = 'date_time <= STR_TO_DATE('.sql_quote(str_replace('/','.',$valeurs['date_max_signature'])).', GET_FORMAT(DATE, "EUR"))';

		if (count($awhere))
			$where .= ' AND '. implode(' AND ',$awhere);

		$valeurs['id_petition'] = intval(_request('id_petition'));

	}

	if ($id_petition = intval(_request('id_petition'))) {
		$where .= ($where?" AND ":"")."{$GLOBALS['connexions'][0]['prefixe']}_signatures.id_petition=".$id_petition;
	}

	# Calcul des valeurs possibles
	$villes = sql_allfetsel('DISTINCT(ville)',"{$GLOBALS['connexions'][0]['prefixe']}_signatures, {$GLOBALS['connexions'][0]['prefixe']}_petitions", ($where ? "$where AND ":"") . "{$GLOBALS['connexions'][0]['prefixe']}_signatures.id_petition={$GLOBALS['connexions'][0]['prefixe']}_petitions.id_petition");
	$valeurs['villes'] = array();
	foreach ($villes as $ville) {
		if ($ville['ville']) $valeurs['villes'][$ville['ville']] = $ville['ville']; 
	}
	asort($valeurs['villes']);

	$p = array();
	$porteurs = sql_allfetsel('DISTINCT(id_porteur)', 'spip_signatures');
	foreach ($porteurs as $value) {
		$p[] = $value['id_porteur'];
	}
	$porteurs = sql_allfetsel('id_auteur,nom',"spip_auteurs","id_auteur IN ('".implode("','", $p)."')");
	foreach ($porteurs as $porteur) {
		$valeurs['porteurs'][$porteur['id_auteur']] = $porteur['nom']; 
	}

	if (isset($valeurs['porteurs'])) asort($valeurs['porteurs']);
	
	$codepostaux = sql_allfetsel('DISTINCT(codepostal)',"{$GLOBALS['connexions'][0]['prefixe']}_signatures, {$GLOBALS['connexions'][0]['prefixe']}_petitions",($where ? "$where AND ":"") . "{$GLOBALS['connexions'][0]['prefixe']}_signatures.id_petition={$GLOBALS['connexions'][0]['prefixe']}_petitions.id_petition");

	$valeurs['codepostaux'] = array();
	$valeurs['departements'] = array();

	foreach ($codepostaux as $codepostal) {
		if ($codepostal['codepostal']) {
			$valeurs['codepostaux'][$codepostal['codepostal']] = $codepostal['codepostal'];
			$departement = substr($codepostal['codepostal'], 0,2);
			$valeurs['departements'][$departement] = $departement; 
		}
	}
	asort($valeurs['codepostaux']);
	asort($valeurs['departements']);

	$valeurs['recherche_doublons'] = _request('recherche_doublons');
	
	$valeurs['_hidden'] .= '<input type="hidden" name="par_page" value="'.addslashes(_request('par_page')).'" />';
	$valeurs['_hidden'] .= '<input type="hidden" name="tri_liste_art" value="'.addslashes(_request('tri_liste_art')).'" />';

	$valeurs['where'] = $where;

	$sql = "SELECT * from {$GLOBALS['connexions'][0]['prefixe']}_signatures WHERE $where";

	$res = sql_query($sql);
	$valeurs['resultat'] = sql_fetch_all($res);

	// Prendre en compte la petition courante
	if (_request('id_petition') and intval(_request('id_petition'))) {
		$valeurs['id_petition'] = intval(_request('id_petition'));
	}
	// Sinon passer au formulaire toutes les pétitions pour un filtre de plus
	$p = array();
	$petitions = sql_allfetsel('id_petition,id_article',"spip_petitions", "", "", "id_petition DESC");
	foreach ($petitions as $petition) {
		$valeurs['petitions'][$petition['id_petition']] = textebrut(couper(sql_getfetsel('titre', 'spip_articles', 'id_article = ' . 
				$petition['id_article']), 40));
	}


	return $valeurs;
}

