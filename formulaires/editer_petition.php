<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/actions');
include_spip('inc/editer');

// http://doc.spip.org/@inc_editer_petition_dist
function formulaires_editer_petition_charger_dist($id_petition='new', $id_article=0, $retour='', $lier_trad=0, $config_fonc='petitions_edit_config', $row=array(), $hidden=''){
	$valeurs = formulaires_editer_objet_charger('petition',$id_petition,$id_article,$lier_trad,$retour,$config_fonc,$row,$hidden);

	$lists = charger_fonction('lists','newsletter');
 	$valeurs['_listes_dispo'] = $lists(array('status'=>'open'));

	return $valeurs;
}

function formulaires_editer_petition_verifier_dist($id_petition='new', $id_article=0, $retour='', $lier_trad=0, $config_fonc='petitions_edit_config', $row=array(), $hidden=''){
    if (!_request('email_unique')) set_request('email_unique', 'non');
    if (!_request('message')) set_request('message', 'non');
	$erreurs = formulaires_editer_objet_verifier('petition',$id_petition,array());
	return $erreurs;
}

// http://doc.spip.org/@inc_editer_petition_dist
function formulaires_editer_petition_traiter_dist($id_petition='new', $id_article=0, $retour='', $lier_trad=0, $config_fonc='petitions_edit_config', $row=array(), $hidden=''){
	$retour = generer_url_ecrire('petition','id_petition='.$id_petition);
    return formulaires_editer_objet_traiter('petition',$id_petition,$id_article,$lier_trad,$retour,$config_fonc,$row,$hidden);
}

// Filtre pour le formulaire qui va afficher le nombre d'abonné à un connecteur Mailshot
function mailshot_list_subscribers($list){
    $subscribers = charger_fonction('subscribers','newsletter');
    return $subscribers(array($list),array('count'=>true));
}