<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/actions');
include_spip('inc/editer');

// http://doc.spip.org/@inc_editer_signature_dist
function formulaires_editer_signature_charger_dist($id_signature='new', $id_petition=0, $retour='', $lier_trad=0, $config_fonc='signatures_edit_config', $row=array(), $hidden=''){
	$valeurs = formulaires_editer_objet_charger('signature',$id_signature,$id_petition,$lier_trad,$retour,$config_fonc,$row,$hidden);

	if ($valeurs['id_porteur']
		OR (!_DIR_RESTREINT AND $id_signature=='new' AND $valeurs['id_porteur']=$GLOBALS['visiteur_session']['id_auteur'])) {
		$porteur = sql_fetsel('*','spip_auteurs',"id_auteur=".$valeurs['id_porteur']);
		$valeurs['email_porteur'] = $porteur['email'];
		$valeurs['nom_porteur'] = $porteur['nom'];
	}
	// Lorsqu'on cree une nouvelle signature, il faut preciser l'id_petition
	if (!$valeurs['id_petition']) $valeurs['id_petition'] = $id_petition;
	return $valeurs;
}

/**
 * Identifier le formulaire en faisant abstraction des parametres qui
 * ne representent pas l'objet edite
 */
function formulaires_editer_signature_identifier_dist($id_signature='new', $id_petition=0, $retour='', $lier_trad=0, $config_fonc='signatures_edit_config', $row=array(), $hidden=''){
		return serialize(array($id_signature,$lier_trad,$row));
}

// Choix par defaut des options de presentation
// http://doc.spip.org/@signatures_edit_config
function signatures_edit_config($row)
{
	global $spip_ecran, $spip_lang, $spip_display;

	$config = $GLOBALS['meta'];
	$config['lignes'] = ($spip_ecran == "large")? 5 : 3;
	$config['langue'] = $spip_lang;

	$config['restreint'] = ($row['statut'] == 'publie');
	return $config;
}

function formulaires_editer_signature_verifier_dist($id_signature='new', $id_petition=0, $retour='', $lier_trad=0, $config_fonc='signatures_edit_config', $row=array(), $hidden=''){
	$erreurs = formulaires_editer_objet_verifier('signature',$id_signature,array('nom_email','firstname','lastname','codepostal','ville'));
	include_spip('base/abstract_sql');
	if (!$erreurs) {
		$doublon = 'F:'._request('firstname').'- L:'._request('lastname').'- CP:'._request('codepostal');
		$n = sql_fetsel('*', 'spip_signatures', "statut!='poubelle' AND CONCAT('F:',firstname,'- L:',lastname,'- CP:',codepostal) = ".sql_quote($doublon)." AND id_signature!=".sql_quote($id_signature));
		if ($n) $erreurs['message_erreur'] = "Erreur : Un doublon a été détecté.\nVous avez déjà certainement déjà signé cette pétition !";
	}

	return $erreurs;
}

// http://doc.spip.org/@inc_editer_signature_dist
function formulaires_editer_signature_traiter_dist($id_signature='new', $id_petition=0, $retour='', $lier_trad=0, $config_fonc='signatures_edit_config', $row=array(), $hidden=''){
	$res = formulaires_editer_objet_traiter('signature',$id_signature,$id_petition,$lier_trad,$retour,$config_fonc,$row,$hidden);
/*	if ($id_signature == 'new' and isset($res['id_signature'])) {
		sql_updateq("spip_signatures", 
			array("statut" => "publie"), 
			"id_signature=".$res['id_signature']);
		$row = sql_fetsel('ad_email', 'spip_signatures', "id_signature=".$res['id_signature']);
		if ($row) {
			$email = $row['ad_email'];
			$row = sql_fetsel('id_liste', 'spip_petitions', "id_petition=$id_petition");
			$id_liste = $row['id_liste'];
			$abonner_auteur_apres_signature = charger_fonction('abonner_auteur_apres_signature','inc');
			$abonner_auteur_apres_signature($email,_request('nom_email'),$id_liste);					
		}
	}*/
	return $res;
}

?>
