<?php

function mespetits_affiche_gauche($flux){
    if (isset($flux["args"]["exec"]) && ($flux["args"]["exec"] == "naviguer")) 
    {
        $flux['data'] .=
            debut_cadre_enfonce('',true)
            . icone_horizontale('Pétitions ', generer_url_ecrire('petitions'), _DIR_PLUGIN_MESPETITS.'img_pack/suivi-petition-24.gif', '', false)
            . icone_horizontale('Signatures ', generer_url_ecrire('controle_petition'), _DIR_PLUGIN_MESPETITS.'img_pack/inscription2-16.png', '', false)
            . fin_cadre_enfonce(true)
            ;            
    }
    if (isset($flux["args"]["exec"]) && ($flux["args"]["exec"] == "petition")) {
        $ret = '';
        
        include_spip('inc/autoriser');
        
        if (autoriser('ecrire', 'signature')) {
            include_spip('inc/presentation'); # pour icone_horizontale
            $ret .= boite_ouvrir('','simple');
            $ret .= icone_horizontale(_T('mespetits:creer_signature'), generer_url_ecrire('signature','id_petition='.$flux['args']['id_petition']), 'signature-24.png', 'creer.gif', false);
            $ret .= boite_fermer();
        }
            
        $flux["data"] .= $ret;
    }

    return $flux;
}


?>