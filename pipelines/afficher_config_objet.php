<?php

/**
 * Boite de configuration des objets articles
 *
 * @param array $flux
 * @return array
 */
function mespetits_afficher_config_objet($flux) {
	if ((($type = $flux['args']['type']) == 'article')
		and ($id = $flux['args']['id'])
	) {

        $ret = '';
        
        include_spip('inc/autoriser');
        
        if (autoriser('ecrire', 'signature') ) {
            include_spip('inc/presentation'); # pour icone_horizontale
            $ret .= boite_ouvrir('','simple');
            $ret .= icone_horizontale(_T('mespetits:creer_signature'), generer_url_ecrire('signature','id_article='.$id), 'signature-24.png', 'creer.gif', false);
            $ret .= boite_fermer();
        }
            
        $flux["data"] .= $ret;

	}

	return $flux;
}
