<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Apres modification d'une petition ou d'une signature
 */
function mespetits_pre_edition($flux) {
	include_spip('base/mespetits');

	// Mise a jour de la petition pour prendre en compte le connecteur a Mailshot-Susbcriber
	if ($flux['args']['table'] == 'spip_petitions') {	
		sql_updateq("spip_petitions", array('liste' => _request('liste')),
			'id_petition='.sql_quote($flux['args']['id_objet'])
		);
	}

	// Mise a jour de la signature 
	// pipeline appelé depuis l'espace privé
	if ($flux['args']['table'] == 'spip_signatures') {

		$champs = mespetits_declarer_champs_extras();
		$maj = array();
		foreach ($champs['spip_signatures'] as $key => $value) {
			if ($m = _request($key)) $maj[$key] = $m;
		}
		if (!intval(_request('id_signature'))) {
			// Création
			$maj['id_porteur'] = $GLOBALS['visiteur_session']['id_auteur'];
		}
		$maj['ip'] = $_SERVER["REMOTE_ADDR"];
		sql_updateq("spip_signatures", $maj,
			'id_signature='.sql_quote($flux['args']['id_objet'])
		);

	}

	return $flux;
}