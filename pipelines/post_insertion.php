<?php

/**
 * Apres l'insertion d'une petition ou d'une signature
 */
function mespetits_post_insertion($flux) {
	// Mise a jour de la petition tout juste inseree pour lui rajouter le connecteur a MailSusbcriber
	if ($flux['args']['table'] == 'spip_petitions') {
		sql_updateq("spip_petitions", array(
			'id_list' => intval(_request('id_list'))),
			'id_petition' =>$flux['args']['id_objet']));
	}

	// Mise a jour de la signature pour lui rajouter un porteur et une IP
	if ($flux['args']['table'] == 'spip_signatures') {
		if ($GLOBALS['visiteur_session']['statut']!='6forum') 
			sql_updateq("spip_signatures", array(
			'id_porteur' => $GLOBALS['visiteur_session']['id_auteur']
			'ip' => $_SERVER["REMOTE_ADDR"]
			),
			'id_signatures' =>$flux['args']['id_objet'])
		);
	}

	return $flux;
}