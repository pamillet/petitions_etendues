<?php

/**
 * Couper le texte a une longueur N donnée
 * Si le texte est plus long que N-6 caractères
 * alors on retourne les (N-6) premiers caractères
 * suivis de l'ellipse.
 *
 * @param string $texte
 *     Texte à couper
 * @param int $taille
 *     Taille de la coupe
 * @param string $suite
 *     Points de suite ajoutés.
 * @return string
 *     Texte coupé
 **/
function couper_court($texte,$taille,$suite=' (...)',$encoding='') {
    include_spip('inc/filtres');

    // Parfois il n'y a rien à faire
    if (!($length=strlen($texte)) OR $taille <= 0) return '';

    // eviter de travailler sur 10ko pour extraire 150 caracteres
    $offset = 400 + 2*$taille;

    while ($offset<$length
            AND strlen(preg_replace(",<(!--|\w|/)[^>]+>,Uims","",substr($texte,0,$offset)))<$taille)
            $offset = 2*$offset;
    if ($offset<$length
        && ($p_tag_ouvrant = strpos($texte,'<',$offset))!==NULL){
            $p_tag_fermant = strpos($texte,'>',$offset);
            if ($p_tag_fermant && ($p_tag_fermant<$p_tag_ouvrant))
                    $offset = $p_tag_fermant+1; // prolonger la coupe jusqu'au tag fermant suivant eventuel
    }
    $texte = substr($texte, 0, $offset);

    // Nettoyage complet
    $texte = textebrut(propre($texte));

    // Faut-il réduire cette chaîne ?
    if (strlen($texte) <= $taille) return $texte;

    // Utiliser l'encodage par défaut ('UTF-8' par exemple) pour bien traiter les accents
    if (!$encoding) $encoding = $GLOBALS['meta']['charset'];

    // réduisons
    // Faut-il rajouter une ellipse en fin de chaîne ?
    $taille_ellipse = strlen(textebrut($suite));
    if (strlen($texte) >= $taille - $taille_ellipse) 
        $texte = trim(mb_substr($texte,0,$taille - $taille_ellipse,$encoding)) . $suite;
    else
        $texte = trim(mb_substr($texte,0,$taille,$encoding));


    // supprimer l'eventuelle entite finale mal coupee
    $texte = preg_replace('/&#?[a-z0-9]*$/S', '', $texte);

    return $texte;
}

