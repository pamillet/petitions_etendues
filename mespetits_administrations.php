<?php
if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/cextras');
include_spip('base/mespetits');
 
function mespetits_upgrade($nom_meta_base_version,$version_cible) {
 
    $maj = array();
    $champs = mespetits_declarer_champs_extras();
    cextras_api_upgrade($champs, $maj['create']);   
     
    include_spip('base/upgrade');
    //die("$nom_meta_base_version, $version_cible");
    maj_plugin($nom_meta_base_version, $version_cible, $maj);
}
 
function mespetits_vider_tables($nom_meta_base_version) {
    cextras_api_vider_tables(mespetits_declarer_champs_extras());
    effacer_meta($nom_meta_base_version);
}

