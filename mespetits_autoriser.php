<?php


function autoriser_signature_modifier($faire, $type, $id, $qui, $opt) {
	// Webmestre
	if (autoriser('webmestre')) return true;

	$row = sql_fetsel('statut,id_porteur','spip_signatures','id_signature='.intval($id));

	if ($GLOBALS['visiteur_session']['statut'] == '0minirezo' AND !in_array($row['statut'],array('poubelle','publie')))
		return true;

	// Porteur qui peut modifier sa signature portée non publiée
	return ($qui['id_auteur']==$row['id_porteur'] && !in_array($row['statut'],array('poubelle','publie')));
}
