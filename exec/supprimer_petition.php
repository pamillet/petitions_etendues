<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) return;

// http://doc.spip.org/@action_editer_article_dist
function exec_supprimer_petition_dist($arg=null) {

	if (!autoriser('webmestre')) {
		include_spip('inc/minipres');
		echo minipres();		
	}

	// Enregistre l'envoi dans la BD
	if ($id_petition=intval(_request('id_petition'))) $err = supprimer_petition($id_petition);

	if (_request('redirect')) {
		$redirect = parametre_url(urldecode(_request('redirect')),
			'id_petition', $id_petition, '&') . $err;

		include_spip('inc/headers');
		redirige_par_entete($redirect);
	}
	else
		return array($id_petition,$err);
}


// Mettre a jour la petition
function supprimer_petition($id_petition) {
	sql_updateq("spip_petitions", array('statut' => 'poubelle'), "id_petition=".sql_quote($id_petition));
	return;
}

?>
