<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/presentation');

// http://doc.spip.org/@exec_petitions_dist
function exec_petitions_dist()
{
	global $connect_statut, $connect_id_auteur;

 	pipeline('exec_init',array('args'=>array('exec'=>'petitions'),'data'=>''));
	$commencer_page = charger_fonction('commencer_page', 'inc');
	echo $commencer_page(_T('mespetits:titre_page_petitions'), "naviguer", "petitions");

	echo debut_gauche('', true);

//
// Afficher le bouton de creation d'une petition
//

	echo pipeline('affiche_gauche',array('args'=>array('exec'=>'petitions'),'data'=>''));

	if (sql_countsel('spip_rubriques')) {
		echo bloc_des_raccourcis(icone_horizontale(_T('mespetits:icone_ecrire_petition'), generer_url_ecrire("petitions_edit","new=oui"), "article-24.gif", "creer.gif", false));
	} else {
		if (autoriser('creerrubriquedans', 'rubrique')) {
			echo _T('texte_creer_rubrique');
			echo	bloc_des_raccourcis(icone_horizontale (_T('icone_creer_rubrique'), generer_url_ecrire("rubriques_edit","new=oui&retour=nav"), "rubrique-24.gif", "creer.gif",false));
		}
	}

	echo creer_colonne_droite('', true);
	echo pipeline('affiche_droite',array('args'=>array('exec'=>'petitions'),'data'=>''));
	echo debut_droite('', true);

	echo afficher_objets('petition',"Liste des p&eacute;titions", array('FROM' => "{$GLOBALS['connexions'][0]['prefixe']}_articles AS articles ", "WHERE" => "1=1", 'ORDER BY' => "{$GLOBALS['connexions'][0]['prefixe']}_petitions.maj DESC"));


	echo pipeline('affiche_milieu',array('args'=>array('exec'=>'petitions'),'data'=>''));

	echo fin_gauche(), fin_page();
}

?>
