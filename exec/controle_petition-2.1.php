<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/presentation');

// http://doc.spip.org/@exec_controle_petition_dist
function exec_controle_petition_dist()
{
	exec_controle_petition_args(intval(_request('id_petition')),
		_request('type'),
		_request('date'),
		intval(_request('debut')),
		intval(_request('id_signature')),
		intval(_request('pas'))); // a proposer grapiquement
}

// http://doc.spip.org/@exec_controle_petition_args
function exec_controle_petition_args($id_petition, $type, $date, $debut, $id_signature, $pas=NULL)
{
	$awhere = array();
	if (_request('recherche_doublons')) $awhere[] = "
	CONCAT( `id_petition` , '_', `ville` , '_', `lastname` , '_', `firstname` ) 
	IN (
	SELECT CONCAT( `id_petition` , '_', `ville` , '_', `lastname` , '_', `firstname` ) AS valeur
	FROM `{$GLOBALS['connexions'][0]['prefixe']}_signatures` 
	GROUP BY valeur
	HAVING COUNT( * ) >1
	)";
	if (_request('id_porteur')) $awhere[] = 'id_porteur='.sql_quote(_request('id_porteur'));
	if (_request('ville')) $awhere[] = "ville = ".sql_quote(_request('ville'));
	if (_request('codepostal')) $awhere[] = 'codepostal = '.sql_quote(_request('codepostal'));
	if (_request('date_signature')) $awhere[] = 'DATE(date_time) = '.sql_quote(_request('date_signature'));
	if (_request('date_min_signature')) $awhere[] = 'date_time >= STR_TO_DATE('.sql_quote(str_replace('/','.',_request('date_min_signature'))).", GET_FORMAT(DATE, 'EUR'))";
	if (_request('date_max_signature')) $awhere[] = 'date_time <= STR_TO_DATE('.sql_quote(str_replace('/','.',_request('date_max_signature'))).", GET_FORMAT(DATE, 'EUR'))";
	if (count($awhere))
		$where = implode(' AND ',$awhere).' AND ';
	else
		$where = '';
	controle_petition_args($id_petition, $type, $date, $debut, $titre, $where, $pas);
}

function controle_petition_args($id_petition, $type, $date, $debut, $titre, $where, $pas)
{
	if (!preg_match('/^\w+$/',$type)) $type = 'public';
	if ($id_petition) $where .= "id_petition=$id_petition AND ";
	$extrait = "(statut='publie')";
	if ($type == 'interne') $extrait = "(statut NOT IN('publie','poubelle'))";
	if ($type == 'deleted') $extrait = "(statut='poubelle')";
	$where .= $extrait;
	$order = "date_time DESC";
	if (!$pas) $pas = 10;
	if ($date) {
		include_spip('inc/forum');
		$query = array('SELECT' => 'UNIX_TIMESTAMP(date_time) AS d',
				'FROM' => $GLOBALS['connexions'][0]['prefixe'].'_signatures', 
				'WHERE' => $where,
				'ORDER BY' => $order);
		$debut = navigation_trouve_date($date, 'd', $pas, $query);
	}
	$signatures = charger_fonction('signatures', 'inc');
	
	$res = $signatures('controle_petition', $id_petition, $debut, $pas, $where, $order, $type);

	if (_AJAX) {
			ajax_retour($res);
	} else {

		if (autoriser('modererpetition')
		OR (
			$id_petition > 0
			AND autoriser('modererpetition', 'petition', $id_petition)
			))
			$ong = controle_petition_onglet($id_petition, $debut, $type);
		else {
		  $type = 'public';
		  $ong = '';
		}
		controle_petition_page($id_petition, $titre, $ong, $res);
	}
}


// http://doc.spip.org/@controle_petition_onglet
function controle_petition_onglet($id_petition, $debut, $type, $arg='')
{
	$arg .= ($id_petition ? "id_petition=$id_petition&" :'');
	$arg2 = ($debut ? "debut=$debut&" : '');
	if ($type=='public') {
	  $argp = $arg2;
	  $argi = '';
	} else {
	  $argi = $arg2;
	  $argp = '';
	}

	return debut_onglet()
	  . onglet(_T('titre_signatures_confirmees'), generer_url_ecrire('controle_petition', $argp . $arg . "type=public"), "public", $type=='public', _DIR_PLUGIN_MESPETITS."img_pack/signature-ok-24.png")
	.  onglet(_T('titre_signatures_attente'), generer_url_ecrire('controle_petition', $argi . $arg .  "type=interne"), "interne", $type=='interne', _DIR_PLUGIN_MESPETITS."img_pack/signature-attente-24.png")
	.  onglet("Signatures supprimées", generer_url_ecrire('controle_petition', $argi . $arg .  "type=deleted"), "deleted", $type=='deleted', _DIR_PLUGIN_MESPETITS."img_pack/signature-supprimee-24.png")
	. fin_onglet()
	. '<br />';
}
?>
