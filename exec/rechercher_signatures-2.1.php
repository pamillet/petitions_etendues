<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('exec/controle_petition');

// http://doc.spip.org/@exec_controle_petition_dist
function exec_rechercher_signatures_dist()
{
	exec_controle_petition_args(intval(_request('id_petition')),
		_request('type'),
		_request('date'),
		intval(_request('debut')),
		intval(_request('id_signature')),
		intval(_request('pas'))); // a proposer grapiquement
}

