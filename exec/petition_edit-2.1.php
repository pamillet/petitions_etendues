<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/presentation');
include_spip('inc/documents');

// http://doc.spip.org/@exec_petition_edit_dist
function exec_petition_edit_dist()
{
	exec_petition_edit_args(intval(_request('id_article')), ((_request('new') == 'oui') ? 'new' : ''));
}

// http://doc.spip.org/@exec_petition_edit_args
function exec_petition_edit_args($id_article, $new)
{
	if (!$new AND (!autoriser('voir', 'article', $id_article) OR !autoriser('modifier','article', $id_article))) {
		include_spip('inc/minipres');
		echo minipres(_T('info_acces_interdit'));
	} else {
		$petition_select = charger_fonction('petition_select','inc');
		$row = $petition_select($id_article ? $id_article : $new);
		$id_rubrique = $row ? $row['id_rubrique'] : false;
		if (!$id_rubrique OR ($new AND !autoriser('creerarticledans','rubrique',$id_rubrique))) {
			include_spip('inc/minipres');
			echo minipres(_T('public:aucun_article'));
		} else petition_edit($id_article, $id_rubrique, $new, 'petition_edit_config', $row);
	}
}

function petition_edit($id_article, $id_rubrique, $new, $config_fonc, $row) {

	$id_article = $row['id_article'];
	//$id_rubrique = $row['id_rubrique'];
	$titre = sinon($row["titre"],_T('info_sans_titre'));
	$commencer_page = charger_fonction('commencer_page', 'inc');
	pipeline('exec_init',array('args'=>array('exec'=>'petition_edit','id_article'=>$id_article),'data'=>''));
	
	echo $commencer_page(_T('titre_page_petition_edit', array('titre' => $titre)), "naviguer", "article", $id_rubrique);

	//echo debut_grand_cadre(true);
	//echo afficher_hierarchie($id_rubrique,'',$id_article,'article');
	//echo fin_grand_cadre(true);

	echo debut_gauche("",true);

	echo pipeline('affiche_gauche',array('args'=>array('exec'=>'petition_edit','id_article'=>$id_article),'data'=>''));
	echo creer_colonne_droite("",true);
	echo pipeline('affiche_droite',array('args'=>array('exec'=>'petition_edit','id_article'=>$id_article),'data'=>''));
	echo debut_droite("",true);
	
	$oups = generer_url_ecrire("petitions");

	# Calcul des valeurs possibles
	$liste = sql_allfetsel('id_liste,titre',"{$GLOBALS['connexions'][0]['prefixe']}_listes","statut!='poublist'");
	$liste_des_listes = array();
	foreach ($liste as $value) {
		$liste_des_listes[$value['id_liste']] = $value['titre'];
	}
	$row['liste_des_listes'] = $liste_des_listes;

	$contexte = array(
	//'icone_retour'=>icone_inline(_T('icone_retour'), $oups, "suivi-petition-24.gif", "rien.gif",$GLOBALS['spip_lang_left']),
	'redirect'=>generer_url_ecrire("petitions"),
	'titre'=>$titre,
	'new'=>$new?$new:$row['id_article'],
	'id_rubrique'=>$new?$id_rubrique:$row['id_rubrique'],
	'id_liste'=>$row['id_liste'],
	'id_article'=>$row['id_article'],
	'liste_des_listes'=>$liste_des_listes,
	'row'=>$new?'':$row
	);

//echo '<pre>';var_dump($contexte);die('ok');
	
	$milieu = recuperer_fond("prive/squelettes/contenu/petition", $contexte);

	echo $milieu;
	
	echo pipeline('affiche_milieu',array('args'=>array('exec'=>'articles_edit','id_article'=>$id_article),'data'=>$milieu));

	echo fin_gauche(), fin_page();

}
?>
