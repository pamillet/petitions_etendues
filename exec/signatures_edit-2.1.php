<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/presentation');
include_spip('inc/actions');
include_spip('formulaires/signature');

// http://doc.spip.org/@exec_signatures_dist
function exec_signatures_edit_dist()
{
	exec_signatures_args(intval(_request('id_signature')),intval(_request('id_petition')),((_request('new') == 'oui') ? 'new' : ''));
}

// http://doc.spip.org/@exec_signatures_args
function exec_signatures_args($id_signature, $id_petition='', $new='')
{
	if (!$new AND !autoriser('modifier','petition', $id_petition)) {
		// Une signature existante ne peut etre modifiée que par un admin et son porteur
		include_spip('inc/minipres');
		echo minipres(_T('info_acces_interdit')." -- DEBUG:: $id_signature");
	} else {
		// Nouvelle signature ou signature existante
		$signature_select = charger_fonction('signature_select','inc');
		$row = $signature_select($id_signature ? $id_signature : $new, $id_petition);
		signatures_edit($id_signature, $row['id_petition'], $new, 'petitions_edit_config', $row);
	}
}

function signatures_edit($id_signature, $id_article, $new, $config_fonc, $row) {
	pipeline('exec_init',array('args'=>array('exec'=>'signatures','id_signature'=>$id_signature),'data'=>''));

	if (!$row
	OR !autoriser('voir', 'article', $id_article)) {
		include_spip('inc/minipres');
		echo minipres(_T('public:aucune_signature'));
		return;
	}
	
	$commencer_page = charger_fonction('commencer_page', 'inc');
	pipeline('exec_init',array('args'=>array('exec'=>'signatures_edit','id_signature'=>$id_signature),'data'=>''));
	
	$titre = $new ? "Nouvelle Signature" : $row['titre'];
	echo $commencer_page(_T('titre_page_petitions_edit', array('titre' => $titre)), "naviguer", "article");

	echo debut_grand_cadre(true);
	echo afficher_hierarchie($row['id_rubrique'],'',$id_article,'article',$row['id_secteur'],($row['statut'] == 'publie'));
	echo fin_grand_cadre(true);

	echo debut_gauche("",true);

	echo pipeline('affiche_gauche',array('args'=>array('exec'=>'petitions_edit','id_article'=>$id_article),'data'=>''));
	echo creer_colonne_droite("",true);
	echo pipeline('affiche_droite',array('args'=>array('exec'=>'petitions_edit','id_article'=>$id_article),'data'=>''));
	echo debut_droite("",true);
	
	$oups = '?exec=controle_petition&id_article='.$id_article;
	
	if (!$new) {
		$row['session_nom'] = '';
		$row['session_email'] = '';
	} else {
		$row['session_nom'] = $GLOBALS['visiteur_session']['nom'];
		$row['session_email'] = $GLOBALS['visiteur_session']['email'];
	}
	
	$contexte = array(
	'icone_retour'=>icone_inline(_T('icone_retour'), $oups, "suivi-petition-24.gif", "rien.gif",$GLOBALS['spip_lang_left']),
	'redirect'=>$oups,
	'titre'=>$titre,
	'new'=>$new?$new:$row['id_signature'],
	'id_signature'=>$row['id_signature'],
	'id_article'=>$id_article,
	'row'=>$new?'':$row
	);

	$milieu = recuperer_fond("prive/editer/signature", $contexte);
	
	echo $milieu;
	
	echo pipeline('affiche_milieu',array('args'=>array('exec'=>'signature_edit','id_signature'=>$id_signature,'id_article'=>$id_article),'data'=>$milieu));

	echo fin_gauche(), fin_page();

}

