<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'ajouter_signature' => 'Ajouter des signatures',
	
	'certains_champs_manquants' => 'Il manque des informations obligatoires',
	'champ_obligatoire' => 'Cette case doit obligatoirement &ecirc;tre renseign&eacute;e',
	'creer_signature' => 'Ajouter une signature',
	
	'form_pet_mail_confirmation' => 'Bonjour,

Vous avez demand&eacute; &agrave; signer la p&eacute;tition :
@titre@.

Vous avez fourni les informations suivantes :
@detail@

    @message@

IMPORTANT...
Pour valider votre signature, il suffit de vous connecter &agrave;
l\'adresse ci-dessous (dans le cas contraire, votre demande
sera rejet&eacute;e) :

    @url@


Merci de votre participation
',
	'form_pet_signature_a_valider' => 'Votre signature a &eacute;t&eacute; envoy&eacute;e. Elle est en cours de validation.',

	'icone_ecrire_petition' => 'Cr&eacute;er une nouvelle p&eacute;tition',
	'info_controle_petitions' => 'Cette partie vous permet de g&eacute;rer les signatures, de les exporter, d&#x27;effectuer des recherches par crit&egrave;re',
	'info_lisibilite' => 'Pour une meilleure lisibilit&eacute; de la liste, veuillez calibrer votre espace priv&eacute; en grande largeur',
	'info_modifier_signature' => "Modifier la signature",
	'info_tous_petitions_presents' => 'Liste des p&eacute;titions :',
	'info_tous_signatures_presents' => 'Liste des signatures :',
	
	'liste_des_signatures' => "Liste des signatures",
	
	'nombre_signatures_un' => '1 signature',
	'nombre_signatures_plus' => '@nb@&nbsp;signatures',
	
	'texte_creer_petition' => 'Cr&eacute;er la p&eacute;tition :',
	'texte_modifier_petition' => 'Modifier la p&eacute;tition :',
	'texte_modifier_signature' => 'Modifier / Cr&eacute;er la signature :',
	'titre_ajouter_un_signature' => 'Nouvelle signature',
	'titre_page_petitions'=>'Les pétitions',
);

?>
