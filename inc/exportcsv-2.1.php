<?php
/*##############################################################
 * ExportCSV
 * Export des articles / rubriques / p�titions SPIP en fichiers CSV.
 *
 * Auteur :
 * St�phanie De Nada� 
 * webdesigneuse.net
 * � 2008 - Distribu� sous licence GNU/GPL
 *
##############################################################*/

include_spip("base/abstract_sql");
include_spip("base/exportcsv_librairie");

# �l�ments d'�criture des lignes CSV ou tableau
global $tr, $l, $g, $gg, $gd, $th, $ht,	$d, $s, $g, $d, $g;
# par d�faut, �l�ments de lignes CSV
$tr = "";
$l = chr(13).chr(10);
$g = $gg = $gd = $th = $ht = '"';
$d = ';';
$s = $g.$d.$g;

# -----------------------------------------------
# -----------------------------------------------
function exportcsv_make($return = true) {
	# �l�ments d'�criture des lignes CSV ou tableau
	global $tr, $l, $g, $gg, $gd, $th, $ht,	$d, $s, $g, $d, $g;
	global $connect_toutes_rubriques, $prefix_t;
	
	$config = lire_config(_PLUGIN_NAME_EXPORTCSV);

# les variables array 
	$secteur = lire_config(_PLUGIN_NAME_EXPORTCSV.'/secteur');
	$rub = lire_config(_PLUGIN_NAME_EXPORTCSV.'/rub');
	$gmc_art = lire_config(_PLUGIN_NAME_EXPORTCSV.'/articles_l_gmc');
	$gmc_rub = lire_config(_PLUGIN_NAME_EXPORTCSV.'/rubriques_d_gmc');
	
# TEST si config faite
	if(is_null($config)) {
		echo _T('exportcsv:erreur_pas_de_config');
		if(!$connect_toutes_rubriques) 
			echo _T('exportcsv:erreur_admin_config');
		else
			echo _T('exportcsv:erreur_lien_config');
		exit;
	}
# TEST si au moins 1 rubrique est configur�e
	if(count($secteur) < 1 && count($rub) < 1) {
		echo _T('exportcsv:erreur_pas_de_rub');
		if(!$connect_toutes_rubriques) 
			echo _T('exportcsv:erreur_admin_config');
		else
			echo _T('exportcsv:erreur_lien_config');
		exit;
	}

	$titre_col = $art_fields = $rub_fields = $rub_mc = $art_mc = $data = array();
	$cpt_col = $j = $k = $n = $x = $y = 0;
	
# intitul� colonnes et groupes de MC
	ksort($config);

	foreach($config as $cle => $val) {
	# dans un 1er temps, on ne prend que les non array pour les titres de colonne
	# car on a pas les noms des groupes de MC , juste leur ID
		if(!is_array($val)) {
		
			if(!is_null($val)) {
				$titre_col[$cpt_col] = preg_replace("/_[a-z]_/", "_", $cle);
#	sdn_debug($titre_col[$cpt_col], 'val '.$cle);
				$cpt_col++;
			}

		} else {
		# dans un 2nd temps, r�cup�res les titres des groupes 
		# de colonne car on a leur ID tri� par type (rub ou art)
			if(preg_match("/^rubriques_/", $cle)) { # si c un array pour les groupes
			# on s�pare les types de groupe : pour rub ou art 
				foreach($val as $clev => $valv) { # Gr. MC rubrique
					$rub_mc[$j] = $valv;
					$j++;
					$r = sql_fetsel('titre', $prefix_t."groupes_mots", "id_groupe='".$valv."'");
					$titre_col[$cpt_col] = $r['titre'];
					$cpt_col++;
sdn_debug($r['titre'], 'titre rub');
				}					
			}
			if(preg_match("/^articles_/", $cle)) {  # Gr. MC article
sdn_debug($cle, 'is_art');
				foreach($val as $clev => $valv) {
					$art_mc[$k] = $valv;
					$k++;

					$select = array('titre');
					$from = array($prefix_t."groupes_mots");
					$where = array("id_groupe='".$valv."'");
					
					$r = sql_fetsel($select, $from, $where);

					$titre_col[$cpt_col] = $r['titre'];
					$cpt_col++;
sdn_debug($r['titre'], 'titre art');
				}
			}
		}
	}
	$nb_col = count($titre_col);

# TEST si au moins 1 champ � afficher est configur�e
	if($nb_col < 1) {
		echo _T('exportcsv:erreur_pas_de_champ');
		if(!$connect_toutes_rubriques) 
			echo _T('exportcsv:erreur_admin_config');
		else
			echo _T('exportcsv:erreur_lien_config');
		exit;
	}	

# �criture de la requete principale
# Tous les articles publi�s dans les secteurs et/ou les rubriques 
	$sel_champs = array("articles.id_rubrique", "articles.id_article");
	$sel_from = array($prefix_t."articles AS `articles`");
	$sel_where = $sel_order = array();

	# les champs � s�lectionner
	for($i = 0; $i < $nb_col; $i++) {
		if(preg_match("/^articles_/", $titre_col[$i])) {
		# pour les articles
			$art_fields[$x] = substr(strstr($titre_col[$i], "_"), 1);
			$x++;
			
			array_push ($sel_champs, preg_replace("/^articles_/", "articles.", $titre_col[$i]));
		}
		elseif(preg_match("/^rubriques_/", $titre_col[$i])) {
		# pour les rubriques
			$rub_fields[$y] = substr(strstr($titre_col[$i], "_"), 1);
			$y++;
		}
	}
#	WHERE
	$where[0] = '(';
	if(count($secteur) > 0) {
		$where[0] .= "(";
		for($i = 0; $i < count($secteur); $i++) {
			if($i > 0)
				$where[0] .= "OR ";
			$where[0] .= "articles.id_secteur = '".$secteur[$i]."' ";
		}
		$where[0] .= ") ";	
	}
	
	if(count($rub) > 0) {
		if(count($secteur) > 0) 
			$where[0] .= "OR ";
		$where[0] .= "( ";
		for($i = 0; $i < count($rub); $i++) {
			if($i > 0)
				$where[0] .= "OR ";
			$where[0] .= "articles.id_rubrique = '".$rub[$i]."' ";
		}
		$where[0] .= ") ";
		
	}
		$where[0] .= ") ";

    $where[0] .= "  (articles.statut = 'publie')";
	
	array_push($sel_order, "articles.id_secteur", "articles.id_rubrique");


	if(!$return) {
		$sel_limit = "0,100";
	}
	else
		$sel_limit = "";
	
#	sdn_debug("<b>SQL :</b> ".$sql);

	
	if(!$return) {	# pour comptage total lignes
		$nb_lignes = sql_countsel($sel_from, $sel_where, $sel_group);
	}
	
	$req = sql_select($sel_champs, $sel_from, $sel_where, '', $sel_order, $sel_limit);
# debug
	ecco_pre($sel_champs, 'sel_champs');
	ecco_pre($sel_from, 'sel_from');
	ecco_pre($sel_where, 'sel_where');
	ecco_pre($sel_order, 'sel_order');

	while($res = sql_fetch($req)) {
# article	
		$id_art = $res['id_article'];
		$id_rub = $res['id_rubrique'];

	# �l�ments de l'article � afficher
		for($i = 0; $i < count($art_fields); $i++) {
		# nettoyage des donn�es (raccourcis typo, etc.)
			$data[$n] = supprimer_numero(textebrut(propre($res[$art_fields[$i]])));
			$n++;
		}
		
	# mots-cl�s article
		if(count($art_mc) > 0) {

			for($i = 0; $i < count($art_mc); $i++) {
				
				$mot = "";
				
				$select = array("mots.titre");
				$from = array($prefix_t."mots_articles AS `L1`", $prefix_t."mots AS `mots`");
				$where = array("(L1.id_article = '".$id_art."') AND (mots.id_groupe = '".$art_mc[$i]."') AND mots.id_mot=L1.id_mot");
				$group = array("mots.id_mot");
				
				$req4 = sql_select($select, $from, $where, $group);
					
				while($res4 = sql_fetch($req4)) {

					$mot .= $res4['titre'].chr(10);
				}
				$data[$n] = substr($mot, 0, -1);
				$n++;
#			sdn_debug( "<hr><b>SQL 4 :</b> ".$sql4);
			}
		}

	# rubrique contenant l'article
		$select = array("rubriques.id_rubrique");
		
		for($i = 0; $i < count($rub_fields); $i++) {				
				
			array_push($select, "rubriques.".$rub_fields[$i]);
		}
		$from = array($prefix_t."rubriques AS `rubriques`");
		$where = array("(rubriques.id_rubrique = '".$id_rub."') AND (rubriques.statut = 'publie')");


		$res2 = sql_fetsel($select, $from, $where);

	# �l�ments de la rubrique � afficher
		for($i = 0; $i < count($rub_fields); $i++) {
		# nettoyage des donn�es (raccourcis typo, etc.)
			$data[$n] = supprimer_numero(textebrut(propre($res2[$rub_fields[$i]])));
			$n++;
		}

	# mots-cl�s pour la rubrique
		if(count($rub_mc) > 0) { # si des mots-cl�s pour rubrique sont s�lectionn�s
		
			for($i = 0; $i < count($rub_mc); $i++) {
				$mot = "";

				$select = array("mots.titre");
				$from = array($prefix_t."mots_rubriques AS `L1`", $prefix_t."mots AS `mots`");
				$where = array("(L1.id_rubrique = '".$id_rub."') AND (mots.id_groupe = '".$rub_mc[$i]."') AND mots.id_mot=L1.id_mot");
				$group = array("mots.id_mot");
				
				$req3 = sql_select($select, $from, $where, $group);

				while($res3 = sql_fetch($req3)) {
					$mot .= $res3['titre'].chr(10);
				}
				$data[$n] = substr($mot, 0, -1);
				$n++;
			}			
		}
		
#		sdn_debug( "<hr><b>SQL 3 :</b> ".$sql3);
		
	}
# DEBUG
	ecco_pre($rub_mc, "rub MC");
	ecco_pre($titre_col, "colonnes");
	ecco_pre($data, "data");
	ecco_pre($config, "exportcsv");
#	sdn_debug(htmlentities($outh));

# �criture du contenu($data) dans un fichier(true) ou tableau(false) selon $return
# initialisation �criture des lignes : 
	# (defaut : extraction vers CSV)
	# extraction pour affichage aper�u en table HTML
	if(!$return) {
		$tr = '<tr>';
		$l = '</tr>';
		$th = '<th>';
		$ht = '</th>';
		$gg = '<td>';
		$gd = '</td>';
		$d = ' ';
		$s = $gd.$gg;
	}
	#	$outh =  entetes (headers))
	#	$outl = lignes de donn�es
	$outh = $tr; $outl = "";

# �criture des titres des colonnes
	for($i = 0; $i < $nb_col; $i++) {
		$outh .= $th.str_replace("_", " ", $titre_col[$i]).$ht.$d;
	}
	$outh = substr($outh, 0, -1).$l;
	
#	$nb_lignes = 0;
	
	for($i = 0; $i < count($data); $i += $nb_col) {
		$outl .= $tr;
		for($z = $i; $z < ($i+$nb_col); $z++) {
			
			$outl .= $gg.$data[$z].$gd.$d;
		}
		$outl = substr($outl, 0, -1).$l;
#		$nb_lignes++;
	}
	$out = $outh.$outl;

	if($return) return $out; # l'export CSV 
	else {
		$nb_lignes > 100 ? $nb_res = 100 : $nb_res = $nb_lignes;
		
		echo '<p style="margin-top:0;"><strong>'._T('exportcsv:info_nb_lignes_a').$nb_res._T('exportcsv:sur_total').$nb_lignes.' :</strong></p>
	<table>'.$out.'</table>'; # affichage de l'export
	}

}

function exportcsv_make_petition($id_article) {
	# �l�ments d'�criture des lignes CSV
	global $tr, $l, $g, $gg, $gd, $th, $ht,	$d, $s, $g, $d, $g, $prefix_t;
# base 
	$titre_col = array('nom_email', 'ad_email', 'date_time', 'firstname','lastname','fonction','adresse1','adresse2','codepostal','ville','organisation','entreprise','profession', 'id_porteur', 'ip', 'message');
	$labels_cols = array('nom_email' => 'Pseudo', 'ad_email' => 'Email', 'date_time'=>'Date', 'firstname' => 'Prenom', 'lastname' => 'Nom',
		'fonction' => 'Fonction', 'adresse1' => 'Adresse1', 'adresse2' => 'Adresse2', 'codepostal' => 'Code postal',
		'ville' => 'Ville', 'organisation' => 'Organisation', 'entreprise' => 'Entreprise', 'profession' => 'Profession', 
		'id_porteur' => 'Porteur', 'ip' => 'Adr. IP', 'message' => 'Message');
	$nb_col = count($titre_col);
	
	$lporteurs = sql_allfetsel('*','spip_auteurs');
	$porteurs = array();
	foreach($lporteurs as $porteur) {
		$porteurs[$porteur['id_auteur']] = $porteur['nom'];
	}
	unset($lporteurs);
	
	$outh = $outl = $tr;
	
	if(is_numeric($id_article) OR _request('q')) {
		$select = $titre_col;
		$from = array($prefix_t."signatures");
		$where = array("id_article=".$id_article." AND statut='publie'");

		if (_request('q')) $where = _request('q');
		
		$req = sql_select($select, $from, $where);
		$num = sql_countsel($from, $where);
		
	# �criture des titres des colonnes
	#	# Ligne 1 CSV = titre de l'article + nombre de signatures
	#	if ($id_article) $req_titre = sql_fetsel("titre", $prefix_t."articles", "id_article=".$id_article);
	#	else $req_titre = array('titre' => "Export des petitions sur requete");

	#	$outh .= 
    # $th.supprimer_numero(textebrut(propre($req_titre['titre']))).$ht.$d
    # .$ht.$num.' signatures'.$ht.$d
    # .$ht.$ht.$d
    # .$ht.$ht
    # .$ht.$ht.$l;
		
		# Ligne 2 CSV = titres des colonnes		
		for($i = 0; $i < $nb_col; $i++) {
			$outh .= $th.$labels_cols[$titre_col[$i]].$ht.$d;
		}
		$outh = substr($outh, 0, -1).$l;
				
		while($res = sql_fetch($req)) {
			$outl .= $tr; 
			
			for($z = 0; $z < $nb_col; $z++) {
				if ($titre_col[$z] == 'id_porteur')
					$outl .= $gg.$porteurs[$res[$titre_col[$z]]].$gd.$d;
				elseif ($titre_col[$z] == 'date_time')
					$outl .= $gg.array_shift(explode(' ',$res[$titre_col[$z]])).$gd.$d;
				else
					$outl .= $gg.$res[$titre_col[$z]].$gd.$d;
			}
			
			$outl = substr($outl, 0, -1).$l;
		}
		$out = $outh.$outl;

		return $out; # l'export CSV 
		
	} else 
		return false;
}
?>