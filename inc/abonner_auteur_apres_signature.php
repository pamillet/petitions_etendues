<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

function inc_abonner_auteur_apres_signature($email,$nom,$liste){

    if (!$id_liste OR !$adresse_email) return;

    // Abonner l'utilisateur à la liste attachee
    // 1. Verifier si un auteur existe avec le mail. 
    // 2. Sinon, on cree un 6forum avec ce mail en login (sans mot de passe)
    // et on abonne le mail
    $row = sql_fetsel('id_auteur', 'spip_auteurs', "email=$adresse_email");
    if (!$row) {

        //include_spip('action/inscrire_auteur');
        include_spip('inc/acces');
        include_spip('inc/editer');
        // generer le mot de passe (ou le refaire si compte inutilise)
        $desc['pass'] = creer_pass_aleatoire(8, $desc['email']);
        $desc['login'] = $desc['email'] = $adresse_email;
        $desc['statut'] = '6forum';
        $desc['nom'] = sinon(_request('nom_email'),$adresse_email);
        $auteur = formulaires_editer_objet_traiter('auteur','new');
                
        include_spip('inc/autoriser');
        // lever l'autorisation pour pouvoir modifier le statut
        autoriser_exception('instituer','auteur',$id_auteur);
        autoriser_exception('modifier','auteur',$id_auteur);
        auteur_modifier($auteur['id_auteur'], $desc);
        autoriser_exception('instituer','auteur',$id_auteur,false);
        autoriser_exception('modifier','auteur',$id_auteur,false);

    }

    $newsletter_subscribe = charger_fonction('subscribe','newsletter');
    if (!$newsletter_subscribe($email,array('nom'=>$nom,'force'=>true,'listes'=>array($liste)))) {
        die(_T('mailsubscriber:erreur_technique_subscribe'));
    }

}