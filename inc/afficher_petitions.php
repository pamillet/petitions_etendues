<?php

//
// Afficher tableau d'articles
//
// http://doc.spip.org/@inc_afficher_articles_dist
function inc_afficher_petitions_dist($titre, $requete, $formater='') {

	if (!isset($requete['FROM'])) $requete['FROM'] = $GLOBALS['connexions'][0]['prefixe'].'_articles AS articles';

	if (!isset($requete['SELECT'])) {
		$requete['SELECT'] = "articles.id_article, articles.titre, articles.id_rubrique, articles.statut, articles.date";
	}

	if (!isset($requete['GROUP BY'])) $requete['GROUP BY'] = '';

	$cpt = sql_countsel($requete['FROM'], $requete['WHERE'], $requete['GROUP BY']);

	if (!$cpt) return '' ;

	$requete['FROM'] = preg_replace("/({$GLOBALS['connexions'][0]['prefixe']}_articles(\s+AS\s+\w+)?)/i", "\\1 JOIN {$GLOBALS['connexions'][0]['prefixe']}_petitions AS petitions ON articles.id_article=petitions.id_article", $requete['FROM']);

	$requete['SELECT'] .= ", petitions.id_article AS petition ";

	// memorisation des arguments pour gerer l'affichage par tranche
	// et/ou par langues.

	$hash = sauver_requete($titre, $requete, $formater);

	if (isset($requete['LIMIT'])) $cpt = min($requete['LIMIT'], $cpt);
	return afficher_petitions_trad($titre, $requete, $formater, $hash, $cpt);
}

// http://doc.spip.org/@afficher_articles_trad
function afficher_petitions_trad($titre_table, $requete, $formater, $hash, $cpt, $trad=0) {

	$fond = "prive/liste/mespetitions";
	if (find_in_path("$fond.html")){
		$contexte = $_GET;
		unset($contexte['where']); // securite
		// passer le where
		foreach($requete as $k=>$v)
			$contexte[strtolower($k)] = $v;
		if (isset($contexte['limit'])){
			$contexte['limit'] = explode(',',$contexte['limit']);
			$contexte['nb'] = end($contexte['limit'])+1;
			unset($contexte['limit']);
		}
		if (isset($contexte['order by'])){
			$contexte['order by'] = explode(' ',$contexte['order by']);
			$sens = (end($contexte['order by'])=='DESC')?-1:1;
			$contexte['order by'] = explode(',',reset($contexte['order by']));
			$contexte['order by'] = explode('.',reset($contexte['order by']));
			$contexte['order'] = end($contexte['order by']);
			if ($contexte['order']=='date')
				$contexte['date_sens'] = $sens;
		}

		#var_dump($contexte);
		$contexte['titre']=$titre;
		$contexte['sinon']=($force ? $titre:'');
		$res = recuperer_fond($fond,$contexte,array('ajax'=>true));
		if (_request('var_liste'))
			var_dump($contexte);
		
		if (!_request('var_liste'))
			return $res;
	}

}

