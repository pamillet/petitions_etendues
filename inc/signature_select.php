<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/autoriser'); // necessaire si appel de l'espace public

// Recupere les donnees d'une signature pour composer un formulaire d'edition

function inc_signature_select($id_signature, $id_petition=0) {

	if (is_numeric($id_signature)) {
		$row = sql_fetsel("*", "{$GLOBALS['connexions'][0]['prefixe']}_signatures,{$GLOBALS['connexions'][0]['prefixe']}_petitions", "id_signature=$id_signature AND {$GLOBALS['connexions'][0]['prefixe']}_signatures.id_petition = {$GLOBALS['connexions'][0]['prefixe']}_petitions.id_petition");
	} else {
        $row = sql_fetsel('id_petition,id_rubrique,id_secteur,statut',"spip_petitions", "id_petition=".intval($id_petition));
    }

	return $row;
}
