<?php
	include_spip('inc/charsets');
	init_mb_string();
	// conversion en utf16LE utilisée par Excel (merci Fil)
	ob_start('convert_utf16');
	function convert_utf16($txt) {
	   return "\xFF\xFE" . mb_convert_encoding(trim($txt), 'UTF-16LE', 'UTF-8');
	}
