<?php

if (!defined("_ECRIRE_INC_VERSION")) return;
 
function mespetits_declarer_champs_extras($champs = array()) {
	$champs['spip_petitions']['liste'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'liste',
			'label' => "Liste",
			'obligatoire' => true,
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array(
				'voir' => array('auteur' => ''),//Tout le monde peut voir
				'modifier' => array('auteur' => 'webmestre')
			),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_signatures']['firstname'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'firstname',
			'label' => "Prénom",
			'obligatoire' => true,
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array(
				'voir' => array('auteur' => ''),//Tout le monde peut voir
				'modifier' => array('auteur' => 'webmestre')
			),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_signatures']['lastname'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'lastname',
			'label' => "Nom",
			'obligatoire' => true,
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
				'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_signatures']['adresse1'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'adresse1',
			'label' => "Adresse 1",
			'sql' => "varchar(255) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_signatures']['adresse2'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'adresse2',
			'label' => "Adresse 2",
			'sql' => "varchar(255) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_signatures']['codepostal'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'codepostal',
			'label' => "Code Postal",
			'obligatoire' => true,
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_signatures']['ville'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'ville',
			'label' => "Ville",
			'obligatoire' => true,
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_signatures']['fonction'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'fonction',
			'label' => "Fonction",
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_signatures']['organisation'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'organisation',
			'label' => "Organisation",
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_signatures']['entreprise'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'entreprise',
			'label' => "Entreprise",
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_signatures']['profession'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'profession',
			'label' => "Profession",
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_signatures']['id_porteur'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'id_porteur',
			'label' => "id_porteur",
			'sql' => "int NOT NULL DEFAULT 0",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_signatures']['ip'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'ip',
			'label' => "Adresse IP",
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);

	$champs['spip_signatures']['np_dans_signataires'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'np_dans_signataires',
			'label' => "Afficher le nom et prénom dans les signataires",
			'sql' => "int(1) NOT NULL DEFAULT 1",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);

	$champs['spip_auteurs']['firstname'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'firstname',
			'label' => "Prénom",
			'obligatoire' => true,
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_auteurs']['lastname'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'lastname',
			'label' => "Nom",
			'obligatoire' => true,
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_auteurs']['adresse1'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'adresse1',
			'label' => "Adresse 1",
			'sql' => "varchar(255) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_auteurs']['adresse2'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'adresse2',
			'label' => "Adresse 2",
			'sql' => "varchar(255) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_auteurs']['codepostal'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'codepostal',
			'label' => "Code Postal",
			'obligatoire' => true,
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_auteurs']['ville'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'ville',
			'obligatoire' => true,
			'label' => "Ville",
			'sql' => "varchar(255) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_auteurs']['fonction'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'fonction',
			'label' => "Fonction",
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_auteurs']['organisation'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'organisation',
			'label' => "Organisation",
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_auteurs']['entreprise'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'entreprise',
			'label' => "Entreprise",
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	$champs['spip_auteurs']['profession'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'profession',
			'label' => "Profession",
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);
	/*$champs['spip_auteurs']['id_porteur'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'id_porteur',
			'label' => "id_porteur",
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);*/
	$champs['spip_auteurs']['ip'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'ip',
			'label' => "Adresse IP",
			'sql' => "varchar(40) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);

	$champs['spip_auteurs']['np_dans_signataires'] = array(
		'saisie' => 'input',//Type du champ (voir plugin Saisies)
		'options' => array(
			'nom' => 'np_dans_signataires',
			'label' => "Afficher le nom et prénom dans les signataires",
			'sql' => "int(1) NOT NULL DEFAULT 1",
			'defaut' => '',// Valeur par défaut
			'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
			'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
		),
	);


	return $champs;	
}
?>
